using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SGC
{
    public class FadeController : MonoBehaviour
    {
        [Header("フェードスピード")]
        [SerializeField] private float fadeSpeed = 0.01f;
        private float alpha;

        [SerializeField] bool isFadeOut = false;
        [SerializeField] bool isFadeIn = false;

        CanvasGroup _CanvasGroup;

        void Start()
        {
            _CanvasGroup = GetComponent<CanvasGroup>();
            alpha = _CanvasGroup.alpha;
        }

        void Update()
        {
            if (isFadeOut)
            {
                StartFadeOut();
            }
            if (isFadeIn)
            {
                StartFadeIn();
            }
        }

        public void StartFadeIn()
        {
            while (alpha > 0)
            {
                alpha -= fadeSpeed;
                SetAlpha();
            }

            if (alpha <= 0)
            {
                //isFadeIn = false;
                //_CanvasGroup.enabled = false;
            }
        }

        public void StartFadeOut()
        {
            //_CanvasGroup.enabled = true;
            while (alpha < 1)
            {
                alpha += fadeSpeed;
                SetAlpha();
            }

            if (alpha >= 1)
            {
                //isFadeOut = false;
            }
        }

        void SetAlpha()
        {
            _CanvasGroup.alpha = alpha;
        }
    }
}
