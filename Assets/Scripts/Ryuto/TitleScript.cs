using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SGC
{
    public sealed class TitleScript : MonoBehaviour
    {
        private void Start()
        {
            SoundService.Instance.PlayBgm("title_bgm_bk");
        }

        public void OnGameStart()
        {
            SoundService.Instance.PlaySe("title_se_dec");
            ViewService.Instance.ChangeView("Story");
        }

        public void OnExplanation()
        {
            SoundService.Instance.PlaySe("title_se_dec");
            ViewService.Instance.ChangeView("Explanation");
        }

        public void OnExit()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
    }
}
