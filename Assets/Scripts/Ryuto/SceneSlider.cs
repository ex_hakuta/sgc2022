using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneSlider : MonoBehaviour
{
    [SerializeField] private GameObject SliderObject;

    private Slider _SceneSlider;

    private void Start()
    {
        _SceneSlider = SliderObject.GetComponent<Slider>();
    }

    private void FixedUpdate()
    {
        _SceneSlider.value = SGC.GameManager.instance.ItemCount;
    }
}
