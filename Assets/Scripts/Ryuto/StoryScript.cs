using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SGC
{
    public sealed class StoryScript : MonoBehaviour
    {
        [SerializeField]
        private GameObject[] page = null;

        private int index;

        [SerializeField]
        private string next = "UITest";

        private void Start()
        {
            SoundService.Instance.PlayBgm("mov_bgm");
            page[0].SetActive(true);
        }

        public void OnButtonClicked()
        {
            this.index++;
            if (this.index >= this.page.Length)
            {
                ViewService.Instance.ChangeView(this.next);
                return;
            }
            SoundService.Instance.PlaySe("mov_se_");
            this.UpdateSprite();
        }

        private void UpdateSprite()
        {
            page[index - 1].SetActive(false);
            page[index].SetActive(true);
        }

        public void OnSkipButton()
        {
            SoundService.Instance.PlaySe("mov_se_skip");
            ViewService.Instance.ChangeView(this.next);
            return;
        }
    }
}
