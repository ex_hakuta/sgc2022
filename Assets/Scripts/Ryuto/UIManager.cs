using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SGC
{
    public class UIManager : MonoBehaviour
    {
        public static UIManager instance;

        [Header("?A?C?e???????J?E???g")]
        [SerializeField] private Text itemCountTextObject;

        private void Awake()
        {
            instance = this;
        }

        public void SetItemText(int itemCount)
        {
            itemCountTextObject.text = $"?{itemCount}";
        }
    }
}
