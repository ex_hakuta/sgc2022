using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SGC
{
    public sealed class GameOverView : MonoBehaviour
    {
        public void OnContinueButtonClicked()
        {
            ViewService.Instance.ChangeView("UITest");
        }

        public void OnTitleButtonClicked()
        {
            ViewService.Instance.ChangeView("Title");
        }
    }
}
