using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SGC
{
    public sealed class ClearView : MonoBehaviour
    {
        public void OnButtonClicked()
        {
            ViewService.Instance.ChangeView("Ending");
        }
    }
}
