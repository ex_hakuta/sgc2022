using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoalScript : MonoBehaviour
{
    private bool isGoal = false;

    private void Update()
    {
        if (isGoal)
        {
            //ほかの処理を止める
            

            //シーン遷移
            SceneManager.LoadScene("ClearScene");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            isGoal = true;
        }
    }
}
