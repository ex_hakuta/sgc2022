using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


namespace SGC
{
    public class ExplanationScript : MonoBehaviour
    {
        [Header("ゲーム説明のテキストオブジェクト")]
        [SerializeField] GameObject GameExplanation;

        [Header("操作説明のテキストオブジェクト")]
        [SerializeField] GameObject OperationExplanation;

        public void OnGameExplanation()
        {
            OperationExplanation.SetActive(false);
            GameExplanation.SetActive(true);
        }

        public void OnOperationExplanation()
        {
            GameExplanation.SetActive(false);
            OperationExplanation.SetActive(true);
        }

        public void OnTitle()
        {
            ViewService.Instance.ChangeView("Title");
        }
    }
}