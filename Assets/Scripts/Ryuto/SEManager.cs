using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SEManager : MonoBehaviour
{
    [SerializeField] AudioClip _ItemGet;

    AudioSource _AudioSource;

    void Start()
    {
        _AudioSource = GetComponent<AudioSource>();
    }

    public void OnItemGetSound()
    {
        _AudioSource.PlayOneShot(_ItemGet);
    }
}
