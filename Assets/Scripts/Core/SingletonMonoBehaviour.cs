using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SGC
{
    public class SingletonMonoBehaviour<T> : MonoBehaviour
    {
        private static T instance;
        public static T Instance { get { return instance; } }

        private void Awake()
        {
            if (instance != null)
            {
                GameObject.Destroy(this.gameObject);
                return;
            }
            instance = this.GetComponent<T>();
        }
    }
}
