using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace SGC
{
    public sealed class SoundService : SingletonMonoBehaviour<SoundService>
    {
        [SerializeField]
        private AudioSource bgmCh = null;

        [SerializeField]
        private AudioSource[] seChs = null;

        public void PlayBgm(string bgmName)
        {
            var asset = Resources.Load<AudioClip>($"Bgm/{bgmName}");
            this.bgmCh.clip = asset;
            this.bgmCh.Play();
        }

        public void PlaySe(string seName)
        {
            var asset = Resources.Load<AudioClip>($"Se/{seName}");

            var seCh = System.Array.Find(this.seChs, i => !i.isPlaying);
            seCh = seCh != null ? seCh : this.seChs[0];
            seCh.PlayOneShot(asset);
        }
    }
}
