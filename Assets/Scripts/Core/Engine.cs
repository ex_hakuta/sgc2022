using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SGC
{
    public sealed class Engine : SingletonMonoBehaviour<Engine>
    {
        [SerializeField]
        private bool isBoot;

        private void Start()
        {
            DontDestroyOnLoad(this.gameObject);

            if (this.isBoot)
            {
                ViewService.Instance.ChangeView("Title");
            }
        }
    }
}
