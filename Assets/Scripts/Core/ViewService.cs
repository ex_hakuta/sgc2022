using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SGC
{
    public sealed class ViewService : SingletonMonoBehaviour<ViewService>
    {
        [SerializeField]
        private float duration = 0.2f;

        [SerializeField]
        private CanvasGroup transition = null;

        public void ChangeView(string viewName)
        {
            this.StartCoroutine(this.InnerChangeView(viewName));
        }
        
        public void AddView(string viewName)
        {
            // TODO:
        }

        private IEnumerator InnerChangeView(string viewName)
        {
            yield return this.FadeIn(this.duration);
            yield return SceneManager.LoadSceneAsync(viewName);
            yield return this.FadeOut(this.duration);
        }
        
        private IEnumerator FadeIn(float duration)
        {
            this.transition.gameObject.SetActive(true);
            this.transition.alpha = 0.0f;
            while (this.transition.alpha < 1.0f)
            {
                this.transition.alpha += Time.deltaTime / duration;
                yield return null;
            }
            this.transition.alpha = 1.0f;
        }

        private IEnumerator FadeOut(float duration)
        {
            this.transition.gameObject.SetActive(true);
            this.transition.alpha = 1.0f;
            while (this.transition.alpha > 0.0f)
            {
                this.transition.alpha -= Time.deltaTime / duration;
                yield return null;
            }
            this.transition.alpha = 0.0f;
            this.transition.gameObject.SetActive(false);
        }
    }
}
