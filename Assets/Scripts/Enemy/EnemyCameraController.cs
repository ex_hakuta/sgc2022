using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace SGC
{

    public class EnemyCameraController : MonoBehaviour
    {
        public enum PatrolState
        {
            Wait,//待機
            Walk,//歩行
        }
        //敵
        [SerializeField] private EnemyBase enemyBase;
        //見回り
        [SerializeField] private bool isPatrol = false;
        public bool IsPatrol { get { return isPatrol; } }
        //位置設定
        private SetPosition setPosition;
        //待機時間
        private float waitTime = 0;
        //最高待機時間
        private float maxWaitTime = 3.0f;
        //状態
        private PatrolState state = PatrolState.Wait;
        private NavMeshAgent agent;
        private const float maxWalkTime = 4.0f;
        private float walkTime = 0.0f;
        private Animator anim;
        //animation　ハッシュ
        private int animWalk = Animator.StringToHash("WalkSpeed");
        private bool isHitCracker = false;
        public bool IsHitCracker { set { isHitCracker = value; } get { return isHitCracker; } }
        //待機時間
        private float elapsedTime = 0;
        //最高待機時間
        private const float maxElapsedTime = 5.0f;
        // Start is called before the first frame update
        void Start()
        {
            anim = GetComponent<Animator>();
            if (isPatrol)
            {
                agent = GetComponent<NavMeshAgent>();
                setPosition = GetComponent<SetPosition>();
                SetState(PatrolState.Wait);
            }
            else
            {
                anim.SetFloat(animWalk, 0);
            }
        }

        // Update is called once per frame
        void Update()
        {
            Patrolling();
            CrackerParalysis();
        }
        //パトロールタイプのカメラ
        void Patrolling()
        {
            //パトロールするか
            if (isPatrol)
            {
                //待機中であれば
                if (state == PatrolState.Wait)
                {
                    waitTime += Time.deltaTime;
                    //待機時間
                    if (waitTime > maxWaitTime || agent.remainingDistance < 0.2f)
                    {
                        setPosition.CreateDestination();
                        agent.SetDestination(setPosition.GetDestination());
                        SetState(PatrolState.Walk);
                        Debug.Log("移動");
                        waitTime = 0;
                    }
                }
                else if (state == PatrolState.Walk)
                {
                    walkTime += Time.deltaTime;
                    if (agent.pathStatus != NavMeshPathStatus.PathInvalid)
                    {
                        //歩行時間に制限を設ける
                        if (walkTime >= maxWalkTime || agent.remainingDistance < 0.1f)
                        {
                            SetState(PatrolState.Wait);
                            walkTime = 0;
                        }
                    }
                    anim.SetFloat(animWalk, agent.desiredVelocity.magnitude);
                    //進んでいる方向を向く
                    transform.LookAt(new Vector3(setPosition.GetDestination().x, transform.position.y, setPosition.GetDestination().z));
                }
            }
        }
        //クラッカーを食らっていれば
        void CrackerParalysis()
        {
            if (isHitCracker)
            {
                elapsedTime += Time.deltaTime;
                agent.isStopped = true;
                Debug.Log("食らった");
                if (elapsedTime > maxElapsedTime)
                {
                    elapsedTime = 0;
                    isHitCracker = false;
                    agent.isStopped = false;
                }
            }
        }
        public void SetState(PatrolState patrolState)
        {
            state = patrolState;
            //停止中
            if (state == PatrolState.Wait)
            {
                anim.SetFloat(animWalk, 0);
                agent.velocity = Vector3.zero;
                agent.isStopped = true;
            }
            //歩行中
            if (state == PatrolState.Walk)
            {
                agent.isStopped = false;
            }
        }
    }
}