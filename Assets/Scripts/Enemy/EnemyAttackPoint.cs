using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttackPoint : MonoBehaviour
{
    [SerializeField] private Collider attackCollider;
    private void Start()
    {
        attackCollider.enabled = false;
    }
    //�U���J�n
    public void AttackStart()
    {
        attackCollider.enabled = true;
        Debug.Log("�U���J�n");
    }
    //�U���I��
    public void AttackEnd()
    {
        attackCollider.enabled = false;
        Debug.Log("�U���I��");
    }
}
