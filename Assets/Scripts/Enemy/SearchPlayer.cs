using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace SGC
{
    //アイテムを集めて目標に近づくと索敵範囲が広がる
    //プレイヤーを探す
    public class SearchPlayer : MonoBehaviour
    {
        public enum PatrolState
        {
            Wait,//待機
            Walk,//歩行
        }
        //敵
        [SerializeField] private EnemyBase enemyBase;
        //笑い声音源
        [SerializeField] private AudioClip laughClip;
        //プレイヤーのタグ
        private string playerTag = "Player";
        //敵のカメラ要因
        private EnemyCameraController enemyCameraController;
        //笑い声ソース
        private AudioSource laughSource;

        private SetPosition setPosition;
        // Start is called before the first frame update
        void Start()
        {
            enemyCameraController = transform.root.GetComponent<EnemyCameraController>();
            setPosition = transform.root.GetComponent<SetPosition>();
            laughSource = GetComponent<AudioSource>();
        }
        private void Update()
        {
            
        }
        private void PlayerFind(Collider other)
        {
            //敵カメラを参照出来ていて、クラッカーが当たった場合
            if (enemyCameraController != null && enemyCameraController.IsHitCracker) return;
            //タグがプレイヤーであれば
            if (other.gameObject.CompareTag(playerTag))
            {
                //状態が追跡以外なら
                if (enemyBase.GetState() != EnemyBase.EnemyState.Chase)
                {
                    enemyBase.SetState(EnemyBase.EnemyState.Chase, other.transform.position);
                    Debug.Log("見つけた");
                    //敵カメラを取得出来たら
                    if (enemyCameraController != null)
                    {
                        //発見の目印(笑い声)
                        laughSource.PlayOneShot(laughClip);
                    }
                }
            }
        }
        private void OnTriggerEnter(Collider other)
        {
            PlayerFind(other);
        }
        /*private void OnTriggerStay(Collider other)
        {
            //タグがプレイヤーであれば
            if (other.gameObject.CompareTag(playerTag))
            {
                //状態が追跡なら
                if (enemyBase.GetState() == EnemyBase.EnemyState.Chase)
                {
                    if(setPosition)
                    setPosition.SetDestination(other.transform.position);
                }
            }
        }*/
        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.CompareTag(playerTag))
            {
                enemyBase.SetState(EnemyBase.EnemyState.Wait,Vector3.zero);
            }
        }
    }
}
