using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SGC
{
    public class SetPosition : MonoBehaviour
    {
        //初期位置
        private Vector3 startPosition;
        //目的地
        private Vector3 destination;
        //目的地
        [SerializeField] private Transform[] targets;
        //目標設定の半径
        [SerializeField] private int destinationRadius = 8;
        //目的地の順番
        [SerializeField] private int order = -1;
        public enum Route { inOrder, random }
        public Route route;
        void Start()
        {
            //　初期位置を設定
            startPosition = transform.position;
            SetDestination(transform.position);
        }
        private void Update()
        {
            
        }
        //　ランダムな位置の作成
        public void CreateRandomPosition()
        {
            //　ランダムなVector2の値を得る
            var randDestination = Random.insideUnitCircle * destinationRadius;
            //　現在地にランダムな位置を足して目的地とする
            SetDestination(startPosition + new Vector3(randDestination.x, 0, randDestination.y));
        }
        public void CreateDestination()
        {
            //順番かランダムに移動するか
            if (route == Route.inOrder)
            {
                CreateInOrderDestination();
            }
            else if (route == Route.random)
            {
                CreateRandomDestination();
            }
        }
        //targetsに設定した順番に作成
        private void CreateInOrderDestination()
        {
            //周回、最大であれば0から
            int multi = order < targets.Length - 1 ? 1 : -1;
            order += multi;
            SetDestination(new Vector3(targets[order].transform.position.x, 0, targets[order].transform.position.z));
        }
        //　targetsからランダムに作成
        private void CreateRandomDestination()
        {
            int num = UnityEngine.Random.Range(0, targets.Length);
            SetDestination(new Vector3(targets[num].transform.position.x, 0, targets[num].transform.position.z));
        }

        //　目的地を設定する
        public void SetDestination(Vector3 position)
        {
            destination = position;
        }

        //　目的地を取得する
        public Vector3 GetDestination()
        {
            return destination;
        }
    }
}
