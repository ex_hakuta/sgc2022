﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace SGC
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class EnemyBase : MonoBehaviour
    {
        public enum EnemyState
        {
            None,
            Wait,//待機
            Attack,//攻撃
            Freeze,//硬直
            Chase,//追跡
            Walk,//歩行
        }
        //目的地
        [SerializeField] private Transform target;
        [SerializeField] private AnimationCurve rotationCurve;
        private NavMeshAgent agent;
        //(敵がプレイヤー、またはアイテムを入手した時変化する)速さの倍率
        private float mulSpeed = 1;
        public float MulSpeed { set { mulSpeed = value; } }
        //移動速度
        private float speed = 0;
        //今の状態
        private EnemyState state;
        //アニメーター
        private Animator anim;
        //animation　ハッシュ
        private int animWalk = Animator.StringToHash("WalkSpeed");
        //最高硬直時間
        private const float maxAttackFreeze = 6.0f;
        //待機時間
        private float elapsedTime = 0f;
        //最高待ち時間        
        private const float waitTime = 5.0f;
        //歩行時間
        private float walkTime = 0.0f;
        //最高歩行時間
        private const float maxWalkTime = 6.0f;
        //位置設定
        private SetPosition setPosition;
        //目的のトランスフォーム
        private Vector3 targetPosition;
        //攻撃時の回転時の速さ
        private float rotateSpeed = 100.0f;
        //プレイヤーがアイテムを入手した
        private bool isPlayerItemGet = false;
        //ランダムに足音
        [SerializeField] AudioClip[] clips;
        //発見時の笑い声
        [SerializeField] AudioClip laughClip;
        //足音
        private AudioSource audioSource;
        //到着した
        private bool arrival = false;
        //現在の回転
        private Quaternion currentRot;
        //回転の速度
        float rotateTime = 0.0f;
        private NavMeshHit navMeshHit;
        //追跡してるかどうか
        private bool isChase = false;

        // Start is called before the first frame update
        void Start()
        {
            agent = GetComponent<NavMeshAgent>();
            anim = GetComponent<Animator>();
            setPosition = GetComponent<SetPosition>();
            audioSource = GetComponent<AudioSource>();
            speed = agent.speed;
            mulSpeed = 1.0f;
            SetState(EnemyState.Walk, Vector3.zero);
        }

        // Update is called once per frame
        void Update()
        {
            PlayerItemGet();
            Move();
            agent.speed = speed * mulSpeed;
        }
        //足音ランダムに
        public void PlayFootstepSE()
        {
            audioSource.PlayOneShot(clips[Random.Range(0, clips.Length - 1)]);
        }

        //状態設定
        public void SetState(EnemyState enemyState, Vector3 targetPos)
        {
            state = enemyState;
            //navmeshは有効であるかどうか
            if (agent.pathStatus != NavMeshPathStatus.PathInvalid)
            {
                //停止中
                if (state == EnemyState.Wait)
                {
                    anim.SetFloat(animWalk, 0);
                    //到着
                    arrival = true;
                    //追跡してない
                    isChase = false;
                    agent.velocity = Vector3.zero;
                    agent.isStopped = true;
                    walkTime = 0;
                    elapsedTime = 0;
                }
                //追跡中
                else if (state == EnemyState.Chase)
                {
                    agent.isStopped = false;
                    arrival = false;
                    mulSpeed = 1.3f;
                    //目的地設定
                    targetPosition = targetPos;
                    //SamplePositionは設定した場所から5の範囲で最も近い距離のBakeされた場所を探す。
                    NavMesh.SamplePosition(targetPosition, out navMeshHit, 5, 1);
                    agent.SetDestination(navMeshHit.position);
                    //発見の合図
                    if (!isChase)
                    {
                        audioSource.PlayOneShot(laughClip);
                        isChase = true;
                    }
                }
                else if (state == EnemyState.Walk)
                {
                    arrival = false;
                    isChase = false;
                    elapsedTime = 0f;
                    //プレイヤーがアイテムを入手した場合
                    mulSpeed = isPlayerItemGet ? 1.2f : 1.0f;
                    //指定した目的地に障害物があるかどうか、そもそも到達可能なのかを確認して問題なければセットする。
                    //pathPending 経路探索の準備できているかどうか
                    if (agent.remainingDistance <= agent.stoppingDistance)
                    {
                        //hasPath エージェントが経路を持っているかどうか
                        //navMeshAgent.velocity.sqrMagnitudeはスピード
                        if (!agent.hasPath || agent.velocity.sqrMagnitude == 0f)
                        {
                            setPosition.CreateRandomPosition();
                        }
                    }
                    if (targetPos != Vector3.zero)
                    {
                        Debug.Log("アイテム");
                        setPosition.SetDestination(targetPos);
                    }
                    agent.SetDestination(setPosition.GetDestination());
                    agent.isStopped = false;

                }
                //攻撃
                else if (state == EnemyState.Attack)
                {
                    anim.SetTrigger("Attack");
                    //audioSource.PlayOneShot(attackSE);
                    agent.velocity = Vector3.zero;
                    isChase = false;
                }
                else if (state == EnemyState.Freeze)
                {
                    elapsedTime = 0f;
                    anim.SetFloat("WalkSpeed", 0);
                    isChase = false;
                    anim.ResetTrigger("Attack");
                }
            }
        }
        public EnemyState GetState()
        {
            return state;
        }
        void NaturalRot()
        {
            var maxRot = transform.rotation.y > currentRot.y ? transform.rotation.y : currentRot.y;
            var minRot = transform.rotation.y > currentRot.y ? currentRot.y : transform.position.y;
            //向きが変わればアニメーションカーブを入れる
            if ((maxRot - minRot) >= 45 && (maxRot - minRot) < 90)
            {
                rotateTime += Time.deltaTime;
                var speed = rotationCurve.Evaluate(rotateTime) * rotateSpeed;
                agent.angularSpeed = speed;
            }
            else
            {
                currentRot = transform.rotation;
                agent.angularSpeed = rotateSpeed;
                rotateTime = 0f;
            }
        }
        //移動
        private void Move()
        {
            //停止中
            if (state == EnemyState.Wait || state == EnemyState.Freeze)
            {
                elapsedTime += Time.deltaTime;
                //歩行開始
                if (elapsedTime >= waitTime)
                {
                    SetState(EnemyState.Walk, Vector3.zero);
                }
            }
            if (state == EnemyState.Walk || state == EnemyState.Chase)
            {
                if (state == EnemyState.Chase)
                {
                    setPosition.SetDestination(targetPosition);
                    agent.SetDestination(setPosition.GetDestination());
                }
                anim.SetFloat(animWalk, agent.desiredVelocity.magnitude);
                if (state == EnemyState.Chase)
                {
                    NaturalRot();
                    if ((targetPosition - transform.position).sqrMagnitude < 0.85f)
                    {
                        SetState(EnemyState.Attack, Vector3.zero);
                    }
                }
                else if (state == EnemyState.Walk)
                {
                    walkTime += Time.deltaTime;
                    //アイテムの場所移動する歩行時間
                    if (walkTime >= maxWalkTime && !arrival || agent.remainingDistance < 0.1f)
                    {
                        SetState(EnemyState.Wait, Vector3.zero);
                        anim.SetFloat(animWalk, 0);
                    }
                    //進んでる方向に向く
                    transform.LookAt(new Vector3(setPosition.GetDestination().x, transform.position.y, setPosition.GetDestination().z));
                }
            }
            //攻撃する時
            else if (state == EnemyState.Attack)
            {
                //　プレイヤーの方向を取得
                var targetDirection = new Vector3(targetPosition.x, transform.position.y, targetPosition.z) - transform.position;
                //　敵の向きをプレイヤーの方向に少しづつ変える
                var dir = Vector3.RotateTowards(transform.forward, targetDirection, rotateSpeed * Time.deltaTime, 0f);
                //　算出した方向の角度を敵の角度に設定
                transform.rotation = Quaternion.LookRotation(dir);
                //硬直状態
                SetState(EnemyState.Freeze, Vector3.zero);
            }
            else if (state == EnemyState.Wait)
            {
                elapsedTime += Time.deltaTime;
                //　待ち時間を越えたら次の目的地を設定
                if (elapsedTime > waitTime)
                {
                    SetState(EnemyState.Walk, Vector3.zero);
                }
                //　攻撃後のフリーズ状態
            }
            else if (state == EnemyState.Freeze)
            {
                elapsedTime += Time.deltaTime;
                if (elapsedTime > maxAttackFreeze)
                {
                    SetState(EnemyState.Walk, Vector3.zero);
                }
            }

        }

        //プレイヤーがアイテムを入手した場合
        void PlayerItemGet()
        {

            //プレイヤーがアイテムを取った場合
            if (GameManager.instance.ItemPos != Vector3.zero)
            {
                if (GameManager.instance.ItemCount > 3)
                    isPlayerItemGet = true;
                audioSource.PlayOneShot(laughClip);
                //待機状態から索敵することもあるので
                agent.isStopped = false;
                SetState(EnemyState.Walk, GameManager.instance.ItemPos);
                //初期化
                GameManager.instance.ItemPos = Vector3.zero;
            }
        }
        //ゲームオーバーの時の停止
        public void Stop()
        {
            enabled = false;
            agent.isStopped = false;
            audioSource.enabled = false;
        }
    }
}
