﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SGC
{
    public class Cracker : MonoBehaviour
    {
        ParticleSystem particle;
        void Start()
        {
            particle = this.GetComponent<ParticleSystem>();
        }

        void Update()
        {
            if (particle.isStopped) //パーティクルが終了したか判別
            {
                Destroy(this.gameObject);//パーティクル用ゲームオブジェクトを削除
            }
        }
        //パーティクルの衝突判定
        private void OnParticleCollision(GameObject other)
        {
            if (other.CompareTag("EnemyCamera"))
            {
                other.GetComponent<EnemyCameraController>().IsHitCracker = true;
                Debug.Log("敵の索敵の敵に当たった");
            }
        }
    }
}