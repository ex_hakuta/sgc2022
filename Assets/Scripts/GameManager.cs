using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SGC
{
    public sealed class GameManager : MonoBehaviour
    {
        public const int MaxItem = 5;

        public enum State : int
        {
            Playing,
            GameClear,
            GameOver,
            Explanation,
        }

        public static GameManager instance;

        [SerializeField]
        private GameObject[] views = null;
        
        [SerializeField]
        private StarterAssets.FirstPersonController player = null;

        [SerializeField]
        private EnemyBase enemy = null;

        private State state;

        //アイテムの数
        public int ItemCount { get; private set; }

        //アイテムを取った場所
        private Vector3 itemPos = Vector3.zero;
        public Vector3 ItemPos { get { return itemPos; } set { itemPos = value; } }
        private bool isHitCracker = false;
        public bool IsHitCracker { set { isHitCracker = value; } get { return isHitCracker; } }

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
        }

        private void Start()
        {
            this.ChangeState(State.Playing);
        }
        public void CountItem()
        {
            this.ItemCount++;
            if (this.ItemCount >= MaxItem)
            {
                this.ToGameClear();
            }
        }

        public void ToGameClear()
        {
            if (this.state == State.GameClear)
            {
                return;
            }
            this.ChangeState(State.GameClear);
        }

        public void ToGameOver()
        {
            if (this.state == State.GameOver)
            {
                return;
            }
            this.player.Stop();
            this.enemy.Stop();
            this.ChangeState(State.GameOver);
        }

        public float GetSpeed()
        {
            return 1.0f;
        }

        public void ChangeState(State state)
        {
            this.state = state;
            foreach (var i in this.views)
            {
                i.gameObject.SetActive(false);
            }
            this.views[(int)state].gameObject.SetActive(true);
        }
    }
}
